/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package textreader.composer;

import java.util.ArrayList;

/**
 *
 * @author Olga
 */
public class Printable implements Print{
    
    ArrayList<Print> mChildPrint = new ArrayList<>();
    
    @Override
     public void print() {
        for (Print part : mChildPrint) {
            part.print();
        }
    }
    
    @Override
    public Printable parse() {
        for (Print part : mChildPrint) {
            part.parse();
        }
        return null;
    }
    
    public int getSize() {
        return mChildPrint.size();
    }
    
    public ArrayList<Print> get() {
        return mChildPrint;
    }
    
    public Print get(int i) {
        if(mChildPrint != null && mChildPrint.size()>i) return mChildPrint.get(i);
        else return null;
    }
    
    public void add(Print part) {
        mChildPrint.add(part);
    }
     
    public void remove(Print part) {
        mChildPrint.remove(part);
    }

    public void removeAll() {
         ArrayList<Print> tmp = new ArrayList<>();
         tmp.addAll(mChildPrint);
         mChildPrint.removeAll(tmp);
    }
    
    public void addAll(ArrayList<Print> tmp) {
        for (Print part : tmp) {
            mChildPrint.add(part);
        }
    }
    
    @Override
    public ArrayList<Print> findUnic() {
        ArrayList<Print> tmp = new ArrayList<>();
        if(mChildPrint==null) return null;
        for (Print part : mChildPrint) {
            ArrayList<Print> tmp1 = part.findUnic();
            tmp.addAll(tmp1);
        }
        return tmp;
    }
    
    @Override
    public String compile() {
        String result = "";
        for (Print part : mChildPrint) {
            result += part.compile();
        }
        return result;
    }
    
    @Override
    public void deleteMaxSubstr(String substring) { 
        for (Print part : mChildPrint) {
            part.deleteMaxSubstr(substring);
        }
    }
}
