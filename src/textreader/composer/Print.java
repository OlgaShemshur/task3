/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package textreader.composer;

import java.util.ArrayList;


/**
 *
 * @author Olga
 */
public interface Print {
    public void print();
    public Printable parse();
    public String compile();
    public void deleteMaxSubstr(String substring);
    public ArrayList<Print> findUnic();
}
