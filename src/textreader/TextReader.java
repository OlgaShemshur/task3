/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package textreader;
        
import java.util.ArrayList;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import textreader.composer.Print;
import textreader.composer.Printable;
import workwithtext.FileWork;
import workwithtext.entities.Text;
/**
 *
 * @author Olga
 */
public class TextReader {
    
    static public String str = "a{1}.*b{1}";
    static{
        new DOMConfigurator().doConfigure("src\\resource\\log4j.xml", LogManager.getLoggerRepository());
    }
    static Logger console = Logger.getLogger(TextReader.class.getName());
    static Logger sort = Logger.getLogger("sort");
    static Logger words = Logger.getLogger("words");
    
    public static void main(String[] args) {
        FileWork fileWork = new FileWork("src\\resource\\Example.txt");
        fileWork.work();
        Text text = new Text(fileWork.getData());
        text.parse();
        text.print();
        ArrayList<Print> tmp = text.findWord();
        for (Print part : tmp) {
            words.info(part);
        }
        Printable p = new Printable();
        p.addAll(tmp);
        console.info(tmp);
        text.deleteMaxSubstr(str);
        text.print();
        ArrayList<Print> tmp1 = text.sort();
        for (Print part : tmp1) {
            sort.info(part);
        }
    }
}
