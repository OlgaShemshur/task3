/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibilitymodel;

import java.util.ArrayList;
import workwithtext.TextType;

/**
 *
 * @author Olga
 */
public abstract class Parser {
    
    private Parser next;
    protected int numberOfDot;
    
    public Parser setNext(Parser parser) {
        next = parser;
        return parser;
    }
    
    public ArrayList<String> parse(String text) {
        int dotCounter = TextType.toType(text);
        if (dotCounter == numberOfDot) {
            return parseText(text);
        }
        if (next != null) {
            ArrayList<String> tmp = next.parse(text);
            if(tmp!= null && tmp.size()!=0) return tmp;
        }
        return null;
    }
    
    abstract protected ArrayList<String> parseText(String parser);
}
