/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibilitymodel;

import static chainofresponsibilitymodel.TextParser.code;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Olga
 */
public class ElemParser extends Parser {
    
    static final String parserElement = "[0-9\\=\\'\\>\\<\\)\\;\\:\\,\\.\\(\\\\\"\\%]+";
    
    public ElemParser(int numberOfDots) {
        this.numberOfDot = numberOfDots;
    }
    
    public ArrayList<String> parseElement(String parser) {
        return parseText(parser);
    }
    
    @Override
    protected ArrayList<String> parseText(String parser) {
        ArrayList<String> elements = new ArrayList<>();
        ArrayList<Integer> indexes = new ArrayList<>();
        Pattern pattern = Pattern.compile(parserElement);
        String tmp ="";
        indexes.add(0);
        for(int i=0; i<parser.length(); i++) {
            Matcher m = pattern.matcher(tmp+parser.charAt(i));
            if(m.matches()) {
                elements.add(tmp+parser.charAt(i));
                if(i!=0) indexes.add(i);
                else indexes.add(i+1);
            }
        }
        indexes.add(parser.length());
        ArrayList<String> result = new ArrayList<>();
        for(int i = 0; i<indexes.size()-1; i++) {
            String s = "";
            s += parser.substring(indexes.get(i), indexes.get(i+1));
            if(!s.isEmpty()) {
                Matcher m = pattern.matcher(s);
                if(!m.matches()) result.add(s);
            }
            if(i<elements.size()) 
                if(!elements.get(i).isEmpty())result.add(elements.get(i));
        }
        return result;
    }
}
