/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibilitymodel;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 *
 * @author Olga
 */
public class WordParser extends Parser {

    static final String parserElement = "\\s+";
    
    public WordParser(int numberOfDots) {
        this.numberOfDot = numberOfDots;
    }

    @Override
    protected ArrayList<String> parseText(String parser) {
        ArrayList<String> mChildPrint = new ArrayList<>();
        Pattern pattern = Pattern.compile(parserElement);
        String[] arr = pattern.split(parser);
        for(String s: arr)
           if(!s.isEmpty()) mChildPrint.add(s);
        return mChildPrint;
    }  
}
