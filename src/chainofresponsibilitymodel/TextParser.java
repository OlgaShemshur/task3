/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibilitymodel;

import java.util.ArrayList;
import java.util.regex.Pattern;
/**
 *
 * @author Olga
 */
public class TextParser extends Parser {
        
   static final String code = "[_]{2,}\\r\\n"; 
    
    public TextParser(int numberOfDots) {
        this.numberOfDot = numberOfDots;
    }
    
    @Override
    public ArrayList<String> parseText(String text) {
        ArrayList<String> mChildPrint = new ArrayList<>();
        Pattern pattern = Pattern.compile(code);
        String[] arr = pattern.split(text);
        for(String s: arr)
           if(!s.isEmpty()) mChildPrint.add(s);
        return mChildPrint;
    }    
}
