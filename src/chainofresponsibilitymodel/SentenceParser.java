/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibilitymodel;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 *
 * @author Olga
 */
public class SentenceParser extends Parser {
    
    static final String space = "(\\r\\n)|\\t";
    static final String dot = "(\\.\\t)|\\.";
    static final String questionMark = "\\?";
    static final String exclamationPoint = "\\!";
    
    public SentenceParser(int numberOfDots) {
        this.numberOfDot = numberOfDots;
    }
     
    @Override
    protected ArrayList<String> parseText(String parser) {
        ArrayList<String> result = new ArrayList<>();
        ArrayList<String> firstParseResult = new ArrayList<>();
        ArrayList<String> secondParseResult = new ArrayList<>();
        ArrayList<String> thirdParseResult = new ArrayList<>();
        boolean isNeedToAddLastSymbol = false;
        Pattern spacePattern = Pattern.compile(space);
        String[] arr = spacePattern.split(parser);
        for(String s: arr)
            if(!s.isEmpty())    firstParseResult.add(s);
        Pattern dotPattern = Pattern.compile(dot);
        for(String s: firstParseResult) {
            if(s.charAt(s.length()-1)=='.') isNeedToAddLastSymbol = true;
            String[] arr1 = dotPattern.split(s);
            for(String s1: arr1)
                if(!s1.isEmpty())    secondParseResult.add(s1+".");
            if(!isNeedToAddLastSymbol) {
                secondParseResult.set(secondParseResult.size()-1, secondParseResult.get(secondParseResult.size()-1).substring(0, secondParseResult.get(secondParseResult.size()-1).length()-1));
                isNeedToAddLastSymbol = true;
            }
            isNeedToAddLastSymbol = false;
        }
        Pattern questionMarkPattern = Pattern.compile(questionMark);
        for(String s: secondParseResult) {
            if(s.charAt(s.length()-1)=='?') isNeedToAddLastSymbol = true;
            String[] arr1 = questionMarkPattern.split(s);
            for(String s1: arr1)
                if(!s1.isEmpty()) thirdParseResult.add(s1+"?");
            if(!isNeedToAddLastSymbol) {
                thirdParseResult.set(thirdParseResult.size()-1, thirdParseResult.get(thirdParseResult.size()-1).substring(0, thirdParseResult.get(thirdParseResult.size()-1).length()-1));
                isNeedToAddLastSymbol = true;
            }
            isNeedToAddLastSymbol = false;
        }
        Pattern exclamationPointPattern = Pattern.compile(questionMark);
        for(String s: thirdParseResult) {
            if(s.charAt(s.length()-1)=='!') isNeedToAddLastSymbol = true;
            String[] arr1 = exclamationPointPattern.split(s);
            for(String s1: arr1)
                if(!s1.isEmpty()) result.add(s1+"!");
            if(!isNeedToAddLastSymbol) {
                result.set(result.size()-1, result.get(result.size()-1).substring(0, result.get(result.size()-1).length()-1));
                isNeedToAddLastSymbol = true;
            }
            isNeedToAddLastSymbol = false;
        }
        return result;
    }   
}