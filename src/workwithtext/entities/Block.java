/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workwithtext.entities;

import chainofresponsibilitymodel.Parser;
import java.util.ArrayList;
import textreader.composer.Print;
import textreader.composer.Printable;
/**
 *
 * @author Olga
 */
public class Block implements Print {

    String text;
    Printable sentences = new Printable();
    Code code = new Code();
    Parser parser;
    
    public Block(Parser parser) {
        text = null;
        this.parser = parser;
    }
    
    public Block(String text, Parser parser) {
        this.text = text;
        this.parser = parser;
        parse();
    }
    
    public void setBlock(String text) {
        this.text = text;
        parse();
    }
    
    public void setCode(String code) {
        if(sentences.getSize()==0) {
            Code tmp = new Code(code);
            this.code = tmp;
        }
    }
    
    @Override
    public void print() {
        
    }

    @Override
    public Printable parse() {
        ArrayList<String> sentencesTmp = new ArrayList<>();
        sentencesTmp.addAll(parser.parse(text));
        for (String part : sentencesTmp) {
            if(!part.isEmpty()) {
                Sentence sentence = new Sentence(parser);
                sentence.setSentence(part);
                boolean tmp = sentence.getSentence().isEmpty();
                if(!tmp)sentences.add(sentence);
            }
        }
        return sentences;
    }
    
    @Override
    public String toString() {
        if(code.getCode().isEmpty())
        return sentences.compile() + "\r\n";
        else return "___________________\r\n" + code.getCode() + "___________________\r\n";
    }

    @Override
    public String compile() {
        return this.toString();
    }
    
    @Override
    public void deleteMaxSubstr(String substring) { 
        sentences.deleteMaxSubstr(substring);
        text = compile();
    }

    public Print get(int i) {
        return sentences.get(i);
    }

    @Override
    public ArrayList<Print> findUnic() {
        return sentences.findUnic();
    }
}

