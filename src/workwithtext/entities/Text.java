/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workwithtext.entities;

import chainofresponsibilitymodel.ElemParser;
import chainofresponsibilitymodel.Parser;
import chainofresponsibilitymodel.SentenceParser;
import chainofresponsibilitymodel.TextParser;
import chainofresponsibilitymodel.WordParser;
import java.util.ArrayList;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import textreader.composer.Print;
import textreader.composer.Printable;
import workwithtext.TextType;
/**
 *
 * @author Olga
 */
public class Text implements Print {

    String fullText;
    Printable blocks = new Printable();
    Parser parser, parser1, parser2, parser3, parser4, parser5;
    
    static{
        new DOMConfigurator().doConfigure("src\\resource\\log4j.xml", LogManager.getLoggerRepository());
    }
    static Logger rebuild = Logger.getLogger("rebuild");
    
    public Text(String fullText) {
        this.fullText = fullText;
        parser = new TextParser(1);
        parser1 = parser.setNext(new WordParser(0));
        parser2 = parser1.setNext(new ElemParser(-1));
        parser3 = parser2.setNext(new SentenceParser(-2));
        parser4 = parser3.setNext(new ElemParser(3));
        parser5 = parser4.setNext(new SentenceParser(4));
    }
    
    public String getText() {
        return fullText;
    }
    
    @Override
    public void print() {
        rebuild.info("\r\n" + this.compile());
    }

    @Override
    public Printable parse() {
        blocks.removeAll();
        ArrayList<String> mChild = new ArrayList<>();
        mChild.addAll(parser.parse(fullText));
        for (String part : mChild) {
            int type = TextType.toType(part);
            Block block = new Block(parser);
            if(type==2) {
                block.setCode(part);
            }
            else {
                block.setBlock(part);
            }
            blocks.add(block);
        }
        return blocks;
    }
    
    @Override
    public String toString() {
        return blocks.compile();
    }

    @Override
    public String compile() {
        return this.toString();
    }

    @Override
    public void deleteMaxSubstr(String str) {
        blocks.deleteMaxSubstr(str);
    }
    
    public ArrayList<Print> findWord() {
        int index1 = 0;
        int index2 = 0;
        boolean flag = false;
        Sentence sentence = null;
        while(blocks.get(index1)!=null) {
            Block block = (Block)blocks.get(index1);
            while(block.get(index2)!=null) {
                sentence = (Sentence)block.get(index2);
                Sentence tmp = (Sentence)block.get(index2+1);
                if(sentence.getSize()>0) {
                    flag = true;
                    break;
                }
                else if(block.get(index2+1)!=null && tmp.getSize()>0) index2 += 2; 
                else index2++;                
            }
            if(flag) break;
            index1++;
        }
        if(sentence==null) return null;
        Printable words = sentence.getWords();
        Printable blocksNew = new Printable();
        boolean flag1 = false;
        for(Print part: blocks.get()) {
            if(flag1) blocksNew.add(part);
            else flag1 = !flag1;
        }
        ArrayList<Print> otherBlocks = blocksNew.findUnic();
        ArrayList<Print> result = new ArrayList<>();
        ArrayList<Print> tmp = words.get();
        for (Print part1 : tmp) {
            if(!otherBlocks.contains(part1))  
            result.add((Print)part1);    
        }
        return result;    
    }
    
    public ArrayList<Print> sort() {
        ArrayList<Print> otherBlocks = blocks.findUnic();
        double index1 = 0;
        double index2 = 0;
        for (int j=0; j<otherBlocks.size(); j++) {
            for (int i=0; i<otherBlocks.size()-1; i++) {
                Word first = (Word)otherBlocks.get(i);
                Word second = (Word)otherBlocks.get(i+1);
                index1 = first.getIndex();
                index2 = second.getIndex();
                if(index1<index2) {
                    otherBlocks.set(i, second);
                    otherBlocks.set(i+1, first);                
                }
            }
        }
        return otherBlocks;
    }

    @Override
    public ArrayList<Print> findUnic() {
        return  null;
    }
}
