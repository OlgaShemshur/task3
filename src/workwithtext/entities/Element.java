/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workwithtext.entities;

import java.util.ArrayList;
import textreader.composer.Print;
import textreader.composer.Printable;
import workwithtext.TextType;

/**
 *
 * @author Olga
 */
public class Element implements Print {
    
    static final String parserElement = "[0-9]";
    String element;
    
    public Element() {
        element = "";
    }
    
    public Element(String element) {
        if(TextType.isElement(element))
            this.element = element;
        else this.element = "";
    }
    
    public void setElement(String element) {
        this.element = element;
    }
    
    @Override
    public void print() {
        
    }

    @Override
    public Printable parse() {
        return null;
    }
    
    @Override
    public String toString() {
        return element;
    }

    @Override
    public String compile() {
        return element;
    }

    @Override
    public void deleteMaxSubstr(String substring) {
    
    }

    @Override
    public ArrayList<Print> findUnic() {
        return null;
    }
}
