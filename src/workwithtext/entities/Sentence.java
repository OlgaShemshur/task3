/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workwithtext.entities;

import chainofresponsibilitymodel.ElemParser;
import chainofresponsibilitymodel.Parser;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import textreader.composer.Print;
import textreader.composer.Printable;
import workwithtext.TextType;

/**
 *
 * @author Olga
 */
public class Sentence implements Print {

    String fullText;
    Printable all = new Printable();
    Printable elements = new Printable();
    Printable words = new Printable();
    Parser parser;
    
    public Sentence(Parser parser) {
        fullText = null;
        this.parser = parser;
    }
    
    public Sentence(String fullText, Parser parser) {
        if(TextType.isSentence(fullText)) {
            this.fullText = fullText;
            this.parse();
        }
        else this.fullText = "";
        this.parser = parser;
    }
    
    public void setSentence(String fullText) {
        this.fullText = fullText;
        this.parse();
    }
    
    public String getSentence() {
        return fullText;
    }
    
    public int getSize() {
        return words.getSize();
    }
    
    @Override
    public void print() {
        
    }

    @Override
    public Printable parse() {
        ArrayList<String> mChild = new ArrayList<>();
        mChild.addAll(parser.parse(fullText));
        for (String part : mChild) {
            int type = TextType.toType(part);
            if(type==3) {
                Word word = new Word();
                word.setWord(part);
                all.add(word);
                words.add(word);
            }
            else if(type==-1){
                Element element = new Element();
                element.setElement(part);
                all.add(element);
                elements.add(element);
            }
            else if(type!=4){
                ArrayList<String> tmp = new ArrayList<>();
                ElemParser parsertmp = new ElemParser(0);
                tmp.addAll(parsertmp.parseElement(part));
                for (String part1 : tmp) {
                    type = TextType.toType(part1);
                    if(type==3) {
                        Word word = new Word();
                        word.setWord(part1);
                        all.add(word);
                        words.add(word);
                    }
                    else if(type==-1){
                        Element element = new Element();
                        element.setElement(part1);
                        all.add(element);
                        elements.add(element);
                    }
                }
            }
        }
        return all;
    }
    
    @Override
    public void deleteMaxSubstr(String substring) { 
        ArrayList<String> allMatches = new ArrayList<>();
        Matcher m = Pattern.compile(substring).matcher(fullText);
        while (m.find()) {
            allMatches.add(m.group());
        }
        int max = 0;
        int index = -1;
        int tmp = 0;
        for (String part : allMatches) {
            if (max<part.length()) {
                max = part.length();
                index = tmp;
            }
            tmp++;
        }
        ArrayList<String> tmp1 = new ArrayList<>();
        ArrayList<String> tmp2 = new ArrayList<>();
        for (String part : allMatches) {
            if (part.contains("(")) {
                tmp1.add(part);
                tmp2.add(part.substring(0, part.indexOf("(")) + "\\(" + part.substring(part.indexOf("(")+1));
            }
        }
        allMatches.removeAll(tmp1);
        allMatches.addAll(tmp2);
        if(max>0) {
            fullText = fullText.replaceAll(allMatches.get(index), "");
            elements.removeAll();
            words.removeAll();
            all.removeAll();
            parse();
        }
    }
    
    @Override
    public String toString() {
        return all.compile();
    }

    @Override
    public String compile() {
        return this.toString();
    }
    
    public Printable getWords() {
        return words;
    }

    @Override
    public ArrayList<Print> findUnic() {
        return words.get();
    }
}