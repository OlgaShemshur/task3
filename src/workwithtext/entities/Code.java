/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workwithtext.entities;

import java.util.ArrayList;
import textreader.composer.Print;
import textreader.composer.Printable;
import workwithtext.TextType;

/**
 *
 * @author Olga
 */
public class Code implements Print {
    
    String code;
    
    public Code() {
        code = "";
    }
    
    public Code(String code) {
        if(TextType.isCode(code))
        this.code = code;
        else this.code = "";
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCode() {
        return code;
    }
    
    @Override
    public void print() {
        
    }

    @Override
    public Printable parse() {
        return null;
    }
    
    @Override
    public String toString() {
        return code;
    }

    @Override
    public String compile() {
        return code;
    }

    @Override
    public void deleteMaxSubstr(String substring) {
    
    }

    @Override
    public ArrayList<Print> findUnic() {
        return null;
    }
}
