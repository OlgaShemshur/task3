/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workwithtext.entities;

import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import textreader.composer.Print;
import textreader.composer.Printable;
import workwithtext.TextType;

/**
 *
 * @author Olga
 */
public class Word implements Print {

    static String vowel= "[euioaEUIOAуеыаоэяиюУЕЫАОЭЯИЮ]";
    String word;
    
    public Word() {
        word = "";
    }
    
    public Word(String word) {
        if(TextType.isWord(word))
            this.word = word;
        else this.word = "";
    }
    
    public void setWord(String word) {
        this.word = word;
    }
    
    public double getIndex() {
        Pattern p = Pattern.compile(vowel);  
        Matcher m = p.matcher(word);  
        double index = 0;
        while (m.find()) {
            index++;
        }
        if(index>0) return index/word.length();
        else return 0;
    }
    
    @Override
    public void print() {
        
    }

    @Override
    public Printable parse() {
        return null;
    }

    @Override
    public String toString() {
        return word;
    }

    @Override
    public String compile() {
        return " " + word;
    }

    @Override
    public void deleteMaxSubstr(String substring) {
    
    }

    @Override
    public ArrayList<Print> findUnic() {
        return null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.word);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Word other = (Word) obj;
        if (!this.word.equals(other.word)) {
            return false;
        }
        return true;
    }    
}
