/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workwithtext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Olga
 */
public class TextType {
    
    static final String element = "[\\=\\;\\>\\<\\d\\?\\,\\-\\!\\%\\.\\'\\(\\)\\:]|[\\.{1,3}]"; 
    static final String word = "([А-Яа-я]+)|([a-zA-Z\\-]+)"; 
    static final String sentence = "[\\s*A-ZА-Я]{1}[\\sA-ZА-Яа-яa-z\\:\\>\\<\\=\\;\\,\\'\\\"\\%\\d\\-\\_\\(\\)\\r\\n]*[\\.\\!\\?\\r\\n]*";
    static final String code = "(listing:){1}[.\\t\\r\\n\\s\\S]*";
    static final String textWithCode = "[.\\t\\r\\n\\s\\S]+(listing:){1}[.\\t\\r\\n\\s\\S]*";
    
    public static int toType(String text) {
        if(text.isEmpty()) return 4;
        if(isWord(text)) return 3;
        if(isCode(text)) return 2;
        if(isContainCode(text)) return 1;
        if(isSentence(text)) return 0; 
        if(isElement(text)) return -1;
        return -2;
    }
    
    public static boolean isElement(String text)  {
       Pattern p = Pattern.compile(element);  
       Matcher m = p.matcher(text);  
       return m.matches(); 
    }
    
    public static boolean isContainCode(String text)  {
       Pattern p = Pattern.compile(textWithCode);  
       Matcher m = p.matcher(text);  
       return m.matches(); 
    }
    
    public static boolean isCode(String text)  {
       Pattern p = Pattern.compile(code);  
       Matcher m = p.matcher(text);  
       return m.matches();
    }
    
    public static boolean isWord(String text)  {
       Pattern p = Pattern.compile(word);  
       Matcher m = p.matcher(text);  
       return m.matches(); 
    }
    
    public static boolean isSentence(String text)  {
        Pattern p = Pattern.compile(sentence);  
        Matcher m = p.matcher(text);  
        return m.matches(); 
    }
}
