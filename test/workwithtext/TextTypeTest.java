/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workwithtext;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Olga
 */
public class TextTypeTest {
    
    /**
     * Test of isElement method, of class TextType.
     */
    @Test
    public void testIsElement() {
        System.out.println("isElement");
        String text = ":";
        boolean expResult = true;
        boolean result = TextType.isElement(text);
        assertEquals(expResult, result);
    }

    /**
     * Test of isContainCode method, of class TextType.
     */
    @Test
    public void testIsContainCode() {
        System.out.println("isContainCode");
        String text = "etyu";
        boolean expResult = false;
        boolean result = TextType.isContainCode(text);
        assertEquals(expResult, result);
    }

    /**
     * Test of isCode method, of class TextType.
     */
    @Test
    public void testIsCode() {
        System.out.println("isCode");
        String text = "ertry";
        boolean expResult = false;
        boolean result = TextType.isCode(text);
        assertEquals(expResult, result);
    }

    /**
     * Test of isWord method, of class TextType.
     */
    @Test
    public void testIsWord() {
        System.out.println("isWord");
        String text = "dcfgvbh";
        boolean expResult = true;
        boolean result = TextType.isWord(text);
        assertEquals(expResult, result);
    }

    /**
     * Test of isSentence method, of class TextType.
     */
    @Test
    public void testIsSentence() {
        System.out.println("isSentence");
        String text = "Kfgbh dfgv cfvgbh js.";
        boolean expResult = true;
        boolean result = TextType.isSentence(text);
        assertEquals(expResult, result);
    }
}
