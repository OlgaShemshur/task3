/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workwithtext.entities;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;
import textreader.composer.Print;

/**
 *
 * @author Olga
 */
public class TextTest {
  
    /**
     * Test of sort method, of class Text.
     */
    @Test
    public void testSort() {
        System.out.println("sort");
        Text instance = new Text("Asd as a ajaa.");
        instance.parse();
        ArrayList<Print> expResult = new ArrayList<>();
        expResult.add(new Word("a"));
        expResult.add(new Word("ajaa"));
        expResult.add(new Word("as"));
        expResult.add(new Word("Asd"));
        ArrayList<Print> result = instance.sort();
        assertEquals(expResult, result);
    }
}
